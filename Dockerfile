FROM alpine:3.11

ENV PUPPETEER_SKIP_CHROMIUM_DOWNLOAD=true

RUN apk update && apk --no-cache add wkhtmltopdf nodejs npm \
    && rm -rf /tmp/* /var/tmp/* /var/cache/apk/* /var/lib/apk/*

RUN npm install -g \
    hackmyresume \
    jsonresume-theme-macchiato-ibic

WORKDIR /resumes

# VOLUME [ "/resumes" ]

# ENTRYPOINT [ "hackmyresume" ]
# CMD [ "build", "/resumes/resume.json", "/resumes/resume.pdf", "-t", "/usr/lib/node_modules/jsonresume-theme-macchiato-ibic" ]
